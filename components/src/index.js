import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail 
                author={faker.name.firstName()}
                timeago="11:00 AM"
                randtext={faker.random.words()}
                avatar={faker.image.avatar()}
                />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                author={faker.name.firstName()}
                timeago="7:00 PM"
                randtext={faker.random.words()}
                avatar={faker.image.avatar()}
                />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                author={faker.name.firstName()}
                timeago="9:00 PM"
                randtext={faker.random.words()}
                avatar={faker.image.avatar()}
                />
            </ApprovalCard>
        </div>
    );
};
ReactDOM.render(<App />, document.querySelector('#root'));