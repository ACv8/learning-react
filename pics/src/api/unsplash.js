import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 1dd8ec77cacc11aae646ab96e4500718f7f554fa64ac8933e5a45a50eaf0a0be'
    }
});